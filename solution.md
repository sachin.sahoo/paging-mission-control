
### chronicle-queue

https://github.com/OpenHFT/Chronicle-Queue


Chronicle Queue persists every single message using a memory-mapped file. This allows us to share messages between processes.

It stores data directly to off-heap memory, therefore, making it free of GC overhead. It is designed for providing low-latency message framework for high-performance applications.

In this quick article, we will look into the basic set of operations.
