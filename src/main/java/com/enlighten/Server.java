package com.enlighten;
import com.enlighten.dto.SatInput;

import java.io.InputStream;
import java.util.List;

public class Server {

    public static void main(String args[]) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("input.csv")){

            List<SatInput> inputs = Helper.readInputFile(is);
            Helper.processSatFeed(inputs);

        }catch (Exception exception) {
            exception.printStackTrace();
        }

    }





}
