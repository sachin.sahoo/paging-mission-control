package com.enlighten.dto;


import com.opencsv.bean.CsvBindByPosition;

import java.io.Serializable;
import java.time.LocalDateTime;

//  <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
public class SatInput implements Serializable {
// 20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT

   @CsvBindByPosition(position = 0)
   private String timestamp;
   @CsvBindByPosition(position = 1)
   private String satelliteId;
   @CsvBindByPosition(position = 2)
   private String redHighLimit;
   @CsvBindByPosition(position = 3)
   private String yellowHighLimit;
   @CsvBindByPosition(position = 4)
   private String yellowLowLimit;
   @CsvBindByPosition(position = 5)
   private String redLowLimit;
   @CsvBindByPosition(position = 6)
   private String rawValue;
   @CsvBindByPosition(position = 7)
   private String component;


//   <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|
//   <raw-value>|<component>


   public String getTimestamp() {
      return timestamp;
   }

   public void setTimestamp(String timestamp) {
      this.timestamp = timestamp;
   }

   public String getSatelliteId() {
      return satelliteId;
   }

   public void setSatelliteId(String satelliteId) {
      this.satelliteId = satelliteId;
   }

   public String getRedHighLimit() {
      return redHighLimit;
   }

   public void setRedHighLimit(String redHighLimit) {
      this.redHighLimit = redHighLimit;
   }

   public String getYellowHighLimit() {
      return yellowHighLimit;
   }

   public void setYellowHighLimit(String yellowHighLimit) {
      this.yellowHighLimit = yellowHighLimit;
   }

   public String getYellowLowLimit() {
      return yellowLowLimit;
   }

   public void setYellowLowLimit(String yellowLowLimit) {
      this.yellowLowLimit = yellowLowLimit;
   }

   public String getRedLowLimit() {
      return redLowLimit;
   }

   public void setRedLowLimit(String redLowLimit) {
      this.redLowLimit = redLowLimit;
   }

   public String getRawValue() {
      return rawValue;
   }

   public void setRawValue(String rawValue) {
      this.rawValue = rawValue;
   }

   public String getComponent() {
      return component;
   }

   public void setComponent(String component) {
      this.component = component;
   }
}
