package com.enlighten.dto;

import com.enlighten.constant.Severity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SatAlert implements Serializable {

        private String satelliteId;
        private Severity severity;
        private String component;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timestamp;


    public SatAlert(SatInput satInput, Severity severity) {

        final String inputDate = satInput.getTimestamp();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
        LocalDateTime dateTime = LocalDateTime.parse(inputDate, formatter);

        String satelliteId = satInput.getSatelliteId();
        String component = satInput.getComponent();
        this.satelliteId = satInput.getSatelliteId();
        this.severity = severity;
        this.component = satInput.getComponent();
        this.timestamp = dateTime;
    }

    public SatAlert(String satelliteId, Severity severity, String component, LocalDateTime timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public SatAlert(SatAlert satAlert) {
        this.satelliteId = satAlert.getSatelliteId();
        this.severity = satAlert.getSeverity();
        this.component = satAlert.getComponent();
        this.timestamp = satAlert.getTimestamp();
    }

    public SatAlert() {
    }



    @JsonIgnore
    public String toJson() {
        SatAlert saClone = new SatAlert(this);
        ObjectMapper Obj = new ObjectMapper();
        String jsonStr = "";
        try {
            jsonStr = Obj.writeValueAsString(this);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonStr;
    }

    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
