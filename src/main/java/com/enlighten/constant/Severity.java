package com.enlighten.constant;

public enum Severity {

    YELLOW_LOW("YELLOW LOW"),
    YELLOW_HIGH("YELLOW HIGH"),
    RED_LOW("RED LOW"),
    RED_HIGH("RED HIGH");

    private final String type;

    Severity(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Severity{" +
                "type='" + type + '\'' +
                '}';
    }
}


