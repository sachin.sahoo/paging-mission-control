package com.enlighten;

import com.enlighten.constant.Severity;
import com.enlighten.dto.SatAlert;
import com.enlighten.dto.SatInput;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class Helper {


    /**
     *  Read inputFile and return list of SatInput
     *
     * @param inputStream
     * @return
     */
    public static List<SatInput> readInputFile(InputStream inputStream) {

        List<SatInput> satInputList = null;

        try {
            satInputList = new CsvToBeanBuilder(new InputStreamReader(inputStream))
                    .withType(SatInput.class)
                    .withSeparator('|')
                    .build()
                    .parse();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return satInputList;
    }


    /**
     *  Print to Console : SatAlert
     *
     * @param inputs
     */
    public static void processSatFeed(List<SatInput> inputs) {
        inputs.parallelStream().forEach( satInput -> {
            Severity severity = getSeverity(satInput);
            if(null != severity) {
                System.out.println(new SatAlert(satInput, severity).toJson());
            }
        });
    }

    /**
     *  Calculate Severity for Component
     *  [RED_LOW YELLOW_LOW   -  YELLOW_HIGH RED_HIGH]
     *
     * @param satInput
     * @return
     */
    public static Severity getSeverity(SatInput satInput) {
        float redHighLimit  = Float.parseFloat(satInput.getRedHighLimit());
        float redLowLimit  = Float.parseFloat(satInput.getRedLowLimit());
        float yellowHighLimit  = Float.parseFloat(satInput.getYellowHighLimit());
        float yellowLowLimit  = Float.parseFloat(satInput.getYellowHighLimit());

        float rawValue = Float.parseFloat(satInput.getRawValue());

        Severity severity = null;
        if(rawValue <= yellowLowLimit  && rawValue > redLowLimit){
            severity = Severity.YELLOW_LOW;

        }else if(rawValue <=  redLowLimit) {
            severity = Severity.RED_LOW;

        } else if(rawValue >= yellowHighLimit  && rawValue < redHighLimit) {
            severity =  Severity.YELLOW_HIGH;

        } else if(rawValue >=  redHighLimit) {
            severity = Severity.RED_HIGH;
        }

        return severity;
    }

}
